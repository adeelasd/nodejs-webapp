const schema =require('mongoose')
var mongoosePaginate = require('mongoose-paginate-v2');
const Schema=schema.Schema;
const products= schema.Schema({
  _id: Schema.Types.ObjectId,
  name:{
    type:String,
    required:true,
  },
  url:{
    type:String,
    required:true,
    unique:true
  },
  description:{
    type:String,
    required:false,
  },
  imageurl:{
    type:String,
    required:true,
  },
  date:{
    type:Date,
    default:Date.now
  },
  Catgories: {
    type:Array,
  },
  price:{
    required:true,
    type:Number
  },
  type:{
    type:String,
    required:true
  }
})
products.plugin(mongoosePaginate);

module.exports=schema.model('products',products)
