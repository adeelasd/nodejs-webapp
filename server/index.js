const express = require('express')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const session=require('express-session');
const fileUpload = require('express-fileupload');
var cookieParser = require('cookie-parser');
const app = express()

//server side components

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  secret: '$eCuRiTy',
}))

app.use(fileUpload());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
const router = require('./api/router');
app.use(router)
//database
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/cart', {useNewUrlParser: true});
mongoose.Promise=global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("connected")
});

mongoose.set('useCreateIndex', true);
// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const {
    host = process.env.HOST || '127.0.0.1',
    port = process.env.PORT || 3000
  } = nuxt.options.server

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
