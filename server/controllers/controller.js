const bcrypt=require('bcryptjs')
const mongoose=require('mongoose')
const Products=require('../model/products')
const Admin=require('../model/admin')
//get date for clean Urls
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!
var yyyy = today.getFullYear();

if (dd < 10) {
  dd = '0' + dd;
}

if (mm < 10) {
  mm = '0' + mm;
}

today = dd + '-' + mm + '-' + yyyy;
// A async function to insert Products data
async function insertproduct(url,title,description,imageurl,price,catagory,type) {
let id =new mongoose.Types.ObjectId();
let data=await Products({
  _id: id,
  url:(url+"-"+ today).replace(/"/gi,"").replace(/ /gi,"-"),
  name:title,
  description:description,
  imageurl:imageurl,
  price:price,
  Catgories:catagory.split(','),
  type:type
})
data.save().then(data=>{
  console.log(data);
})
}

async function init(username,password) {
  let data=Admin({
    username:username,
    password:await bcrypt.hash(password,8)
  })
  data.save()
}


module.exports={insertproduct,init}
