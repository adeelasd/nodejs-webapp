const Router = require('express').Router();
const Products=require('../model/products');
const insertProduct=require('../controllers/controller').insertproduct;
Router.post("/api/products",(req,res)=>{
  insertProduct(req.body.url,
    req.body.title,
    req.body.description,
    req.body.imageurl,req.body.price,req.body.catagory,
    req.body.type)
    res.send("products saved")
})
Router.get("/api/products",(req,res)=>{
  const options = {
    page: 1,
    limit: 10,
	collation: {
		locale: 'en'
	}
};
  Products.paginate({},options,(err,result)=>{
    res.json(result)
  })
})
Router.get("/api/products/:id",(req,res)=>{
  const options = {
    page: req.params.id,
    limit: 10,
	collation: {
		locale: 'en'
	}
};
  Products.paginate({},options,(err,result)=>{
    res.json(result)
  })
})
Router.get("/api/product/:id",(req,res)=>{
  Products.findOne({url:req.params.id},(err,data)=>{
    res.json(data)
  })
})
Router.patch("/api/product",(req,res)=>{
  Products.findOneAndUpdate({url:req.body.url},{$set:{[req.body.tobechanged]:req.body.data}},(err,res)=>{
if(err) return err;
console.log(res,req.body.data,req.body.url);

  })
  res.json("Data updated")

})
Router.delete("/api/product",(req,res)=>{
Products.findOneAndDelete({url:req.body.url},(err,respnse)=>{
  if(err) return err;
})
res.json("Data deleted")
})


module.exports=Router
