const Router = require('express').Router();
const Admin=require('../model/admin')
const init=require('../controllers/controller').init
Router.get('/init',(req,res,next)=>{
Admin.findOne({},(err,response)=>{
  if(err) return err
  if(response){
    res.status(404).send("route does not exists")
  }else{
    next()
  }
})
})
Router.post('/api/init',(req,res,next)=>{
  Admin.findOne({},(err,response)=>{
    if(err) return err
    if(response){
      res.status(404)
    }else{
      init(req.body.username,req.body.password)
      next()
    }
  })
  })


module.exports=Router
