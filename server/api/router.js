const Router = require('express').Router();
const productroute=require('./produsts')
const initroute=require('./init')
const login=require('./login')
Router.use(productroute,initroute,login)
module.exports=Router
